module.exports = [
  {
    type: 'item',
    isHeader: true,
    name: 'MAIN NAVIGATION'
  },
  {
    type: 'item',
    icon: 'fa fa-home',
    name: 'Dashboard',
    router: {
      name: 'Home'
    }
  },
  {
    type: 'tree',
    icon: 'fa fa-user',
    name: 'Administrar Produtos',
    items: [
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'Cadastrar produto',
        router: {
          name: 'CadastrarProduto'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'Total vendido',
        router: {
          name: 'TotalVendido'
        }
      }
    ]
  },
  {
    type: 'tree',
    icon: 'fa fa-user',
    name: 'Administrar Vendas',
    items: [
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'Vender produto',
        router: {
          name: 'VenderProduto'
        }
      }
    ]
  }
]
