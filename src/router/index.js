import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from 'components/Dashboard.vue'
import Login from 'components/Login.vue'
import CadastrarProduto from 'components/CadastrarProduto.vue'
import VenderProduto from 'components/VenderProduto.vue'
import TotalVendido from 'components/TotalVendido.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/cadastrar-produto',
      name: 'CadastrarProduto',
      component: CadastrarProduto
    },
    {
      path: '/vender-produto',
      name: 'VenderProduto',
      component: VenderProduto
    },
    {
      path: '/total-vendido',
      name: 'TotalVendido',
      component: TotalVendido
    }
  ],
  linkActiveClass: 'active'
})
